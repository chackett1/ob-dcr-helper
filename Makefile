build:
	go build -o bin/register cmd/register/*.go


.PHONY: register
register: build
	./bin/register

.PHONY: fmt
fmt:
	go fmt ./...