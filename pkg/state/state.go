package state

// AppSate is a very basic Singleton to hold application state
// As the user navigates around the application.
type AppState struct {
	ASPSPOIDCWellKnownEndpoint    string
	ASPSPOrganisationID           string
	ASPSPIssuedClientID           string
	TPPOrganisationID             string
	TPPSoftwareID                 string
	TPPSigningKeyID               string
	TPPSigningKey                 []byte
	TPPTransportKey               []byte
	TPPTransportCert              []byte
	TPPSoftwareStatementAssertion string
	DCRClaimAudience              string
	TPPRedirectURLs               []string
	TPPResponseTypes              []string
	TPPGrantTypes                 []string
}
