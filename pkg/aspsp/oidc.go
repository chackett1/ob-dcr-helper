package aspsp

import (
	"encoding/json"
	"github.com/pkg/errors"
	"io/ioutil"
	"net/http"
	"time"
)

type OIDCMetadata struct {
	//Version                                    interface{}   `json:"version"`
	Issuer                                     string   `json:"issuer"`
	AuthorizationEndpoint                      string   `json:"authorization_endpoint"`
	TokenEndpoint                              string   `json:"token_endpoint"`
	JwksURI                                    string   `json:"jwks_uri"`
	ScopesSupported                            []string `json:"scopes_supported"`
	ResponseTypesSupported                     []string `json:"response_types_supported"`
	GrantTypesSupported                        []string `json:"grant_types_supported"`
	AcrValuesSupported                         []string `json:"acr_values_supported"`
	SubjectTypesSupported                      []string `json:"subject_types_supported"`
	IDTokenSigningAlgValuesSupported           []string `json:"id_token_signing_alg_values_supported"`
	RequestObjectSigningAlgValuesSupported     []string `json:"request_object_signing_alg_values_supported"`
	TokenEndpointAuthMethodsSupported          []string `json:"token_endpoint_auth_methods_supported"`
	TokenEndpointAuthSigningAlgValuesSupported []string `json:"token_endpoint_auth_signing_alg_values_supported"`
	ClaimsSupported                            []string `json:"claims_supported"`
	ClaimsParameterSupported                   bool     `json:"claims_parameter_supported"`
	RequestParameterSupported                  bool     `json:"request_parameter_supported"`
	RequestURIParameterSupported               bool     `json:"request_uri_parameter_supported"`
	RequireRequestURIRegistration              bool     `json:"require_request_uri_registration"`
	RegistrationEndpoint                       string   `json:"registration_endpoint"`
}

func QueryWellKnownEndpoint(endpoint string) (OIDCMetadata, error) {
	c := http.Client{
		Timeout: time.Duration(time.Second * 30),
	}
	req, err := http.NewRequest(http.MethodGet, endpoint, nil)
	if err != nil {
		return OIDCMetadata{}, errors.Wrap(err, "create http request")
	}
	resp, err := c.Do(req)
	if err != nil {
		return OIDCMetadata{}, errors.Wrap(err, "http client.Do()")
	}
	defer resp.Body.Close()
	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return OIDCMetadata{}, errors.Wrap(err, "http response ReadAll()")
	}

	var meta OIDCMetadata
	err = json.Unmarshal(respBytes, &meta)
	if err != nil {
		return OIDCMetadata{}, errors.Wrap(err, "unmarshal bytes to JSON")
	}
	return meta, nil
}
