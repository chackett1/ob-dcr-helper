package aspsp

import (
	"bitbucket.org/chackett1/ob-dcr-helper/pkg/state"
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
	"github.com/satori/go.uuid"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

// Client provides access to ASPSP
type Client struct {
	transportKey  []byte
	transportCert []byte
}

// As per https://openbanking.atlassian.net/wiki/spaces/DZ/pages/937066600/Dynamic+Client+Registration+-+v3.1#DynamicClientRegistration-v3.1-OBClientRegistrationRequest1
type dynamicRegistrationClaim struct {
	jwt.StandardClaims
	TokenEndpointAuthSignatureAlg string   `json:"token_endpoint_auth_signing_alg,omitempty"`
	RequestEncryptAlg             string   `json:"request_object_encryption_alg,omitempty"`
	RequestEncryptEnc             string   `json:"request_object_encryption_enc,omitempty"`
	GrantTypes                    []string `json:"grant_types,omitempty"`
	SubjectType                   string   `json:"subject_type,omitempty"`
	ApplicationType               string   `json:"application_type,omitempty"`
	RedirectUris                  []string `json:"redirect_uris,omitempty"`
	TokenEndpointAuthMethod       string   `json:"token_endpoint_auth_method,omitempty"`
	Scope                         string   `json:"scope,omitempty"`
	RequestObjectSigningAlg       string   `json:"request_object_signing_alg,omitempty"`
	ResponseTypes                 []string `json:"response_types,omitempty"`
	IDTokenSignedResponseAlg      string   `json:"id_token_signed_response_alg,omitempty"`
	SoftwareID                    string   `json:"software_id,omitempty"`
	SoftwareStatement             string   `json:"software_statement,omitempty"`
	ClientID                      string   `json:"client_id,omitempty"`
	TLSClientAuthDN               string   `json:"tls_client_auth_dn,omitempty"`
}

type DynamicRegistrationResponse struct {
	ClientID string `json:"client_id"`
}

func parseAlgorithm(algsSupported []string, label string) (jwt.SigningMethod, error) {
	// Determine what signature algorithm we will use
	for _, alg := range algsSupported {
		sm := jwt.GetSigningMethod(alg)
		if sm != nil {
			return sm, nil
		}
	}
	return nil, fmt.Errorf("no supported algorithm found for `%s`", label)
}
func (a Client) RegisterClient(appState state.AppState, oidcMetadata OIDCMetadata) (string, error) {
	a.transportCert = appState.TPPTransportCert
	a.transportKey = appState.TPPTransportKey

	claimID, _ := uuid.NewV4()
	issuedAt := time.Now()

	requestSignMethod, err := parseAlgorithm(oidcMetadata.RequestObjectSigningAlgValuesSupported, "request_object_signing_alg")
	if err != nil {
		return "", errors.Wrapf(err, "parseAlgorithm(%s)", "request_object_signing_alg")
	}
	tokenEndpointAuthSigningMethod, err := parseAlgorithm(oidcMetadata.TokenEndpointAuthSigningAlgValuesSupported, "token_endpoint_auth_signing_alg")
	if err != nil {
		return "", errors.Wrapf(err, "parseAlgorithm(%s)", "token_endpoint_auth_signing_alg")
	}

	claims := dynamicRegistrationClaim{
		//RequestEncryptAlg: "RSA-OAEP-256",
		//RequestEncryptEnc: "A128CBC-HS256",
		StandardClaims: jwt.StandardClaims{
			Id:        claimID.String(),
			IssuedAt:  issuedAt.Unix(),
			ExpiresAt: issuedAt.Unix() + 60*15, // Expire 15 minutes after creation of this claim
			Issuer:    appState.TPPSoftwareID,
			// Audience: I think that this should be the Org ID on OB directory (per spec)
			// if that is the case, remove the explicit appState/config item.
			Audience: appState.DCRClaimAudience,
		},
		// Doesn't work with RBS but it should
		//Scope:                    "openid accounts payments fundsconfirmations",
		TokenEndpointAuthMethod:       oidcMetadata.TokenEndpointAuthMethodsSupported[0],
		GrantTypes:                    appState.TPPGrantTypes,
		ResponseTypes:                 appState.TPPResponseTypes,
		IDTokenSignedResponseAlg:      jwt.SigningMethodPS256.Alg(),
		RequestObjectSigningAlg:       requestSignMethod.Alg(),
		SoftwareID:                    appState.TPPSoftwareID,
		SoftwareStatement:             appState.TPPSoftwareStatementAssertion,
		TokenEndpointAuthSignatureAlg: tokenEndpointAuthSigningMethod.Alg(),
		SubjectType:                   "public",
		ApplicationType:               "web",
		RedirectUris:                  appState.TPPRedirectURLs,
		ClientID:                      appState.TPPSoftwareID,
	}

	if claims.TokenEndpointAuthMethod == "tls_client_auth" {
		claims.TLSClientAuthDN = a.buildClientDN(appState)
	}

	pk, err := jwt.ParseRSAPrivateKeyFromPEM(appState.TPPSigningKey)
	if err != nil {
		return "", errors.Wrap(err, "parse RSA key")
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	if token == nil {
		return "", errors.New("failed to create JWT with claims")
	}
	token.Header["kid"] = appState.TPPSigningKeyID

	signedDCRRequest, err := token.SignedString(pk)
	if err != nil {
		return "", errors.Wrap(err, "token.signedString()")
	}
	respClientID, err := a.submitRegistration(oidcMetadata.RegistrationEndpoint, signedDCRRequest)
	if err != nil {
		return "", errors.Wrap(err, "aspsp submitRegistration")
	}

	fmt.Println("====================================================")
	fmt.Println("- Registration request:")
	fmt.Println("----------------------------------------------------")
	fmt.Println(signedDCRRequest)
	fmt.Println("====================================================")

	return respClientID, nil
}

func (a Client) buildClientDN(appState state.AppState) string {
	return fmt.Sprintf("C=GB/O=OpenBanking/OU=%s/CN=%s", appState.TPPOrganisationID, appState.TPPSoftwareID)
}

func (a Client) submitRegistration(endpoint, request string) (string, error) {
	headers := map[string]string{
		"Content-Type": "application/jwt",
		"Accept":       "application/json",
	}

	resp, err := a.httpRequest(http.MethodPost, endpoint, []byte(request), headers)
	if err != nil {
		return "", errors.Wrap(err, "http request")
	}
	fmt.Println("====================================================")
	fmt.Println("- Registration response:")
	fmt.Println("----------------------------------------------------")
	fmt.Println(string(resp))
	fmt.Println("====================================================")
	var dcrResp DynamicRegistrationResponse
	err = json.Unmarshal(resp, &dcrResp)
	if err != nil {
		return "", errors.Wrap(err, "json unmarshal DCR Response")
	}

	return dcrResp.ClientID, nil
}

// Ping check the client is configured correctly to make requests
// If configured correct, nil is returned, if not, the error will be populated
// with message with a hint towards the misconfiguration
func (a Client) Ping(endpoint string) error {
	// Test MATLS setup according to ForgeRock
	// https://backstage.forgerock.com/knowledge/openbanking/article/a20594123
	//endpoint := "https://rs.aspsp.ob.forgerock.financial/open-banking/mtlsTest"
	//resp, err := a.httpRequest(http.MethodGet, endpoint, nil, nil)
	//if err != nil {
	//	return errors.Wrap(err, "http request")
	//}
	//
	//type Authority struct {
	//	Authority string `json:"authority"`
	//}
	//
	//type ResponseType struct {
	//	IssuerID    string      `json:"issuerId"`
	//	Authorities []Authority `json:"authorities"`
	//}
	//
	//var respObj ResponseType
	//err = json.Unmarshal(resp, &respObj)
	//if err != nil {
	//	return errors.Wrap(err, "json unmarshal response")
	//}
	//
	//if strings.Contains(strings.ToLower(respObj.IssuerID), "no-cert") {
	//	return errors.New("client certificate not received")
	//} else if strings.Contains(strings.ToLower(respObj.IssuerID), "unknown-cert") {
	//	return errors.New("client certificate is not signed by Open Banking")
	//} else if len(respObj.Authorities) == 1 {
	//	return errors.New("client certificate is signed by Open Banking, but not on-boarded to this ASPSP")
	//} else if len(respObj.Authorities) > 1 {
	//	for _, a := range respObj.Authorities {
	//		if a.Authority == "PISP" || a.Authority == "AISP" {
	//			// On-boarded as an AISP or ASPSP - this is a successful result
	//			return nil
	//		}
	//	}
	//}
	//return errors.New("unsuccessful unknown")
	return nil
}

func (a Client) RequestAccountAccess(oidcMeta OIDCMetadata, appState state.AppState) (string, error) {
	j, _, err := a.buildAccountRequestJWT(appState)
	if err != nil {
		return "", errors.Wrap(err, "build account request JWT")
	}

	// Use the j to build an auth / consent URL
	v := url.Values{}
	v.Set("client_id", appState.ASPSPIssuedClientID)
	v.Set("redirect_uri", "https://127.0.0.1:8443/conformancesuite/callback")
	v.Set("response_type", "code id_token")
	v.Set("scope", "openid")
	v.Set("state", "nonce")
	v.Set("nonce", "nonce")
	v.Set("request", j)
	fmt.Println("--")
	fmt.Println(v.Encode())
	fmt.Println("--")
	url := fmt.Sprintf("%s?%s", oidcMeta.AuthorizationEndpoint, v.Encode())

	return url, nil
}

func (a Client) buildAccountRequestJWT(appState state.AppState) (string, string, error) {
	accountAccessToken, err := a.requestAccountAccessToken(appState)
	if err != nil {
		return "", "", errors.Wrap(err, "request aspsp access token")
	}

	fmt.Printf("Account access token request ID: %s\n", accountAccessToken)

	type Claim struct {
		Value     string `json:"value"`
		Essential bool   `json:"essential"`
	}

	type IDToken struct {
		ACR                 Claim `json:"acr"`
		OpenBankingIntentID Claim `json:"openbanking_intent_id"`
	}

	type UserInfo struct {
		OpenBankingIntentID Claim `json:"openbanking_intent_id"`
	}

	type Claims struct {
		IDToken  IDToken  `json:"id_token"`
		UserInfo UserInfo `json:"user_info"`
	}

	claimID, _ := uuid.NewV4()
	issuedAt := time.Now()
	type AccessClaim struct {
		jwt.StandardClaims
		Scope        string `json:"scope"`
		ClientID     string `json:"client_id"`
		ResponseType string `json:"response_type"`
		RedirectURI  string `json:"redirect_uri"`
		State        string `json:"state"`
		Nonce        string `json:"none"`
		//OpenBankingIntentID string `json:"open_banking_intent_id"`
		Claims Claims `json:"claims"`
	}
	//nonce, _ := uuid.NewV4()
	nonce := "nonce"
	claims := AccessClaim{
		StandardClaims: jwt.StandardClaims{
			Id:        claimID.String(),
			IssuedAt:  issuedAt.Unix(),
			ExpiresAt: issuedAt.Unix() + 60*15, // Expire 15 minutes after issuance
			Issuer:    appState.ASPSPIssuedClientID,
			Subject:   appState.ASPSPIssuedClientID,
			Audience:  "https://matls.as.aspsp.ob.forgerock.financial/oauth2/openbanking",
		},
		Scope:        "openid accounts",
		ClientID:     appState.ASPSPIssuedClientID,
		ResponseType: "code id_token",
		RedirectURI:  "https://127.0.0.1:8443/conformancesuite/callback",
		State:        nonce,
		Nonce:        nonce,
		Claims: Claims{
			IDToken: IDToken{
				OpenBankingIntentID: Claim{
					Essential: true,
					Value:     accountAccessToken,
				},
				ACR: Claim{
					Essential: true,
					Value:     "urn:openbanking:psd2:sca",
				},
			},
			UserInfo: UserInfo{
				OpenBankingIntentID: Claim{
					Essential: true,
					Value:     "urn:openbanking:psd2:sca",
				},
			},
		},
	}

	pk, err := jwt.ParseRSAPrivateKeyFromPEM(appState.TPPSigningKey)
	if err != nil {
		return "", "", errors.Wrap(err, "parse RSA key")
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	if token == nil {
		return "", "", errors.New("failed to create JWT with claims")
	}
	token.Header["kid"] = appState.TPPSigningKeyID

	signedJWT, err := token.SignedString(pk)
	if err != nil {
		return "", "", errors.Wrap(err, "sign JWT")
	}
	fmt.Printf("Signed JWT String: %s\n", signedJWT)

	return signedJWT, accountAccessToken, nil
}

func (a Client) requestAccountAccessToken(appState state.AppState) (string, error) {
	accessToken, err := a.requestASPSPAccessToken(appState)
	if err != nil {
		return "", errors.Wrap(err, "request account access token")
	}
	fmt.Printf("ASPSP Access token: %s\n", accessToken)

	//finID, _ := uuid.NewV4()
	inID, _ := uuid.NewV4()
	idemKey, _ := uuid.NewV4()

	headers := map[string]string{
		"Accept":                           "application/json",
		"Authorization":                    fmt.Sprintf("Bearer %s", accessToken),
		"Content-Type":                     "application/json",
		"x-fapi-customer-ip-address":       "127.0.0.1",
		"x-fapi-customer-last-logged-time": time.Now().String(),
		"x-fapi-financial-id":              "0015800001041REAAY",
		"x-fapi-interaction-id":            inID.String(),
		"x-idempotency-key":                idemKey.String(),
	}

	endpoint := "https://rs.aspsp.ob.forgerock.financial:443/open-banking/v2.0/account-requests"

	data := map[string]interface{}{
		"Permissions": []string{
			"ReadAccountsDetail",
			"ReadBalances",
			"ReadBeneficiariesDetail",
			"ReadDirectDebits",
			"ReadProducts",
			"ReadStandingOrdersDetail",
			"ReadTransactionsCredits",
			"ReadTransactionsDebits",
			"ReadTransactionsDetail",
			"ReadOffers",
			"ReadPAN",
			"ReadParty",
			"ReadPartyPSU",
			"ReadScheduledPaymentsDetail",
			"ReadStatementsDetail",
		},
		"ExpirationDateTime":      "2018-12-31T00:00:00+00:00",
		"TransactionFromDateTime": "2018-01-01T00:00:00+00:00",
		"TransactionToFromTime":   "2018-12-31T00:00:00+00:00",
	}

	risk := map[string]string{}

	payload := map[string]interface{}{
		"Data": data,
		"Risk": risk,
	}

	payloadBytes, err := json.Marshal(payload)

	resp, err := a.httpRequest(http.MethodPost, endpoint, payloadBytes, headers)
	if err != nil {
		return "", errors.Wrap(err, "request account access token")
	}
	fmt.Printf("Account access token response: %s\n", string(resp))

	type responseType struct {
		Data struct {
			AccountRequestID string    `json:"AccountRequestId"`
			Status           string    `json:"Status"`
			CreationDateTime time.Time `json:"CreationDateTime"`
			Permissions      []string  `json:"Permissions"`
		} `json:"Data"`
	}
	var respObj responseType
	err = json.Unmarshal(resp, &respObj)
	if err != nil {
		return "", errors.Wrap(err, "json unmarshal response")
	}

	fmt.Printf("AccountRequestID: %s\n", respObj.Data.AccountRequestID)

	return respObj.Data.AccountRequestID, nil
}

func (a Client) requestASPSPAccessToken(appState state.AppState) (string, error) {
	claimID, _ := uuid.NewV4()
	issuedAt := time.Now()
	claims := jwt.StandardClaims{
		Id:        claimID.String(),
		IssuedAt:  issuedAt.Unix(),
		ExpiresAt: issuedAt.Unix() + 60*15, // Expire 15 minutes after issuance
		Issuer:    appState.ASPSPIssuedClientID,
		Subject:   appState.ASPSPIssuedClientID,
		Audience:  "https://matls.as.aspsp.ob.forgerock.financial/oauth2/openbanking",
	}

	pk, err := jwt.ParseRSAPrivateKeyFromPEM(appState.TPPSigningKey)
	if err != nil {
		return "", errors.Wrap(err, "parse RSA key")
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	if token == nil {
		return "", errors.New("failed to create JWT with claims")
	}
	token.Header["kid"] = appState.TPPSigningKeyID

	clientAssertion, err := token.SignedString(pk)

	payload := url.Values{}
	payload.Add("grant_type", "client_credentials")
	payload.Add("scope", "openid accounts payments")
	payload.Add("client_assertion_type", "urn:ietf:params:oauth:client-assertion-type:jwt-bearer")
	payload.Add("client_assertion", clientAssertion)
	payload.Add("client_id", appState.ASPSPIssuedClientID)

	endpoint := "https://matls.as.aspsp.ob.forgerock.financial/oauth2/realms/root/realms/openbanking/access_token"
	body := []byte(payload.Encode())
	headers := map[string]string{"Content-Type": "application/x-www-form-urlencoded"}
	resp, err := a.httpRequest(http.MethodPost, endpoint, body, headers)
	fmt.Println(string(resp))
	type responseType struct {
		AccessToken string `json:"access_token"`
	}
	var respObj responseType
	err = json.Unmarshal(resp, &respObj)
	if err != nil {
		return "", errors.Wrap(err, "json unmarshal response")
	}

	return respObj.AccessToken, nil
}

func (a Client) httpRequest(method, endpoint string, body []byte, headers map[string]string) ([]byte, error) {
	clientCert, err := tls.X509KeyPair(a.transportCert, a.transportKey)
	tlsConfig := tls.Config{
		Certificates:       []tls.Certificate{clientCert},
		InsecureSkipVerify: true,
		MinVersion:         tls.VersionTLS11,
		MaxVersion:         tls.VersionTLS13,
	}
	transport := http.Transport{
		TLSClientConfig: &tlsConfig,
	}
	client := http.Client{
		Transport: &transport,
		Timeout:   time.Duration(time.Second * 30),
	}

	req, err := http.NewRequest(method, endpoint, bytes.NewReader(body))
	if err != nil {
		return nil, errors.Wrap(err, "http NewRequest")
	}

	// TODO MATLS client option
	// https://medium.com/@sirsean/mutually-authenticated-tls-from-a-go-client-92a117e605a1

	// Set some default headers before setting the provided headers, allowing option
	// for caller to override defaults
	req.Header.Set("Cache-Control", "no-cache")

	if headers != nil {
		for k, v := range headers {
			req.Header.Set(k, v)
		}
	}

	res, err := client.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "http client.Do()")
	}

	resBody, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		return nil, errors.Wrap(err, "http read response ioutil.ReadAll()")
	}

	return resBody, nil
}
