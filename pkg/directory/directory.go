package directory

import "fmt"

type Directory struct{}

func (d Directory) TestMATLS() error {
	fmt.Println("*****************************************************************")
	fmt.Println("*****************************************************************")
	fmt.Println()
	fmt.Println()
	fmt.Println("MATLS test not implemented - will pass")
	fmt.Println()
	fmt.Println()
	fmt.Println("*****************************************************************")
	fmt.Println("*****************************************************************")
	return nil
}

func (d Directory) ListASPSs() error {
	fmt.Println("*****************************************************************")
	fmt.Println("*****************************************************************")
	fmt.Println()
	fmt.Println()
	fmt.Println("List ASPSPs not implemented")
	fmt.Println()
	fmt.Println()
	fmt.Println("*****************************************************************")
	fmt.Println("*****************************************************************")
	return nil
}

func (d Directory) AcquireAccessToken(ssID, signingKID, tokenURL, jwt, scopes string) (string, error) {
	fmt.Println("*****************************************************************")
	fmt.Println("*****************************************************************")
	fmt.Println()
	fmt.Println()
	fmt.Println("Acquire access token not implemented")
	fmt.Println()
	fmt.Println()
	fmt.Println("*****************************************************************")
	fmt.Println("*****************************************************************")
	return "", nil
}

func (d Directory) BuildJWT() (string, error) {
	fmt.Println("*****************************************************************")
	fmt.Println("*****************************************************************")
	fmt.Println()
	fmt.Println()
	fmt.Println("Build JWT not implemented")
	fmt.Println()
	fmt.Println()
	fmt.Println("*****************************************************************")
	fmt.Println("*****************************************************************")
	return "", nil
}

func (d Directory) SignJWT(jwt string) (string, error) {
	fmt.Println("*****************************************************************")
	fmt.Println("*****************************************************************")
	fmt.Println()
	fmt.Println()
	fmt.Println("Sign JWT not implemented")
	fmt.Println()
	fmt.Println()
	fmt.Println("*****************************************************************")
	fmt.Println("*****************************************************************")
	return "", nil
}

func (d Directory) CreateSSA(accessToken string) (string, error) {
	fmt.Println("*****************************************************************")
	fmt.Println("*****************************************************************")
	fmt.Println()
	fmt.Println()
	fmt.Println("Create SSA not implemented")
	fmt.Println()
	fmt.Println()
	fmt.Println("*****************************************************************")
	fmt.Println("*****************************************************************")
	return "", nil
}
