package main

type Config struct {
	ASPSPOrganisationID        string   `json:"aspspOrganisationID"`
	ASPSPOIDCWellKnownEndpoint string   `json:"aspspOIDCWellKnownEndpoint"`
	TPPOrganisationID          string   `json:"tppOrganisationID"`
	TPPSoftwareID              string   `json:"tppSoftwareID"`
	TPPSigningKID              string   `json:"tppSigningKID"` // Use the signing and encryption key
	TPPSigningKeyPath          string   `json:"tppSigningKeyPath"`
	TPPTransportKeyPath        string   `json:"tppTransportKeyPath"`
	TPPTransportCertPath       string   `json:"tppTransportCertPath"`
	TPPSSA                     string   `json:"tppSSA"`
	DCRClaimAudience           string   `json:"dcrClaimAudience"`
	TPPRedirectURLs            []string `json:"tppRedirectURLs"`
	TPPResponseTypes           []string `json:"tppResponseTypes"`
	TPPGrantTypes              []string `json:"tppGrantTypes"`
}
