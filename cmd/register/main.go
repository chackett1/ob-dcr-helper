package main

import (
	"bitbucket.org/chackett1/ob-dcr-helper/pkg/aspsp"
	"bitbucket.org/chackett1/ob-dcr-helper/pkg/state"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/manifoldco/promptui"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"os"
	"strings"
)

var flagDebug bool
var appState state.AppState
var ASPSPOIDCMetadata aspsp.OIDCMetadata
var bankClient aspsp.Client

var menuItems []MenuItem
var menuList promptui.Select

type MenuAction func() error
type MenuItem struct {
	Label       string
	Description string
	Action      MenuAction
}

func main() {
	flag.BoolVar(&flagDebug, "debug", false, "Debug mode")
	flag.Parse()

	appState = state.AppState{}
	buildMenu()

	if flagDebug {
		debugAppFlow()
		fmt.Println("Quitting application flow - debug mode")
		os.Exit(0)
	}

	fmt.Println("------------------------------------------------")
	fmt.Println("Open Banking Dynamic Client Registration Helper")
	fmt.Println("")
	fmt.Println("This tool will help you TODO TODO TODO TODO TODO")
	fmt.Println("")
	fmt.Println("------------------------------------------------")

	// Run the menu and perform actions indefinitely or until an
	// action exits the application.
	for {
		idx, _, err := menuList.Run()
		if err != nil {
			logrus.WithError(err).Fatal("list.Run()")
		}
		err = menuItems[idx].Action()
		if err != nil {
			logrus.WithError(err).Fatal(".Action()")
		}
	}
}

func directoryEnrollment() error {
	fmt.Println(`Please perform the manual tasks within the Open Banking Directory, including the creation
of a Software Statement.

You will need the following values, returned from these manual tasks:
- Organisation ID (string)
- Software Statement ID (string)
- Signing Key ID (kid) (string)
- Transport key (string, path to file)
- Transport cert (string, path to file)
- Signing key (string, path to file)

You will be prompted for this information now`)

	var err error
	prompt := promptui.Prompt{Label: "Organisation ID "}
	input, err := prompt.Run()
	printError(err)
	appState.ASPSPOrganisationID = strings.TrimSpace(input)

	prompt = promptui.Prompt{Label: "Software Statement ID "}
	input, err = prompt.Run()
	printError(err)
	appState.TPPSoftwareID = strings.TrimSpace(input)

	prompt = promptui.Prompt{Label: "Signing Key ID (kid) "}
	input, err = prompt.Run()
	printError(err)
	appState.TPPSigningKeyID = strings.TrimSpace(input)

	prompt = promptui.Prompt{Label: "Transport Key Path "}
	input, err = prompt.Run()
	printError(err)
	tspBytes, err := ioutil.ReadFile(strings.TrimSpace(input))
	if err != nil {
		return errors.Wrap(err, "Read transport key path")
	}
	appState.TPPTransportKey = tspBytes

	prompt = promptui.Prompt{Label: "Transport Cert Path "}
	input, err = prompt.Run()
	printError(err)
	tspCertBytes, err := ioutil.ReadFile(strings.TrimSpace(input))
	if err != nil {
		return errors.Wrap(err, "Read transport cert path")
	}
	appState.TPPTransportCert = tspCertBytes

	prompt = promptui.Prompt{Label: "Signing Key Path "}
	input, err = prompt.Run()
	printError(err)
	skBytes, err := ioutil.ReadFile(strings.TrimSpace(input))
	if err != nil {
		return errors.Wrap(err, "Read signing key path")
	}
	appState.TPPSigningKey = skBytes

	prompt = promptui.Prompt{Label: "SSA [Software Statement Assertion] (encoded format) "}
	input, err = prompt.Run()
	printError(err)
	appState.TPPSoftwareStatementAssertion = strings.TrimSpace(input)

	fmt.Println("Thank you")

	return nil
}

func dynamicRegistration() error {
	fmt.Println("Dynamic registration registers this application as a TPP with ASPSP")

	bankClient = aspsp.Client{}

	// Register with Client
	oidc, err := aspsp.QueryWellKnownEndpoint(appState.ASPSPOIDCWellKnownEndpoint)
	if err != nil {
		return errors.Wrap(err, "aspsp.QueryWellKnownEndpoint()")
	}
	ASPSPOIDCMetadata = oidc
	cID, err := bankClient.RegisterClient(appState, oidc)
	if err != nil {
		return errors.Wrap(err, "client RegisterClient")
	}
	appState.ASPSPIssuedClientID = cID

	fmt.Printf("Did register as TPP with ASPSP. Client ID = %s\n", cID)

	return nil
}

func userAuthenticationConsent() error {
	str, err := bankClient.RequestAccountAccess(ASPSPOIDCMetadata, appState)
	if err != nil {
		return errors.Wrap(err, "bankClient.RequestAccountAccess()")
	}
	fmt.Println(str)
	return nil
}

func accountAccess() error {
	// Print various options...
	return nil
}

func exit() error {
	fmt.Println("Goodbye..")
	os.Exit(0)
	return nil
}

func printError(err error) {
	if err != nil {
		fmt.Println(err)
	}
}

func loadDirectoryConfig() error {
	return loadDirectoryConfigFile("")
}

func loadDirectoryConfigFile(filename string) error {
	if filename == "" {
		prompt := promptui.Prompt{Label: "Configuration file name"}
		f, err := prompt.Run()
		filename = f
		printError(err)
	}

	path := fmt.Sprintf("config/%s", filename)
	fileBytes, err := ioutil.ReadFile(path)
	if err != nil {
		return errors.Wrap(err, "read config file")
	}

	cfg := Config{}
	err = json.Unmarshal(fileBytes, &cfg)
	if err != nil {
		return errors.Wrap(err, "unmarshal json")
	}

	bytes, err := ioutil.ReadFile(strings.TrimSpace(cfg.TPPSigningKeyPath))
	if err != nil {
		return errors.Wrap(err, "Read signing key path")
	}
	appState.TPPSigningKey = bytes

	bytes, err = ioutil.ReadFile(strings.TrimSpace(cfg.TPPTransportKeyPath))
	if err != nil {
		return errors.Wrap(err, "Read transport key path")
	}
	appState.TPPTransportKey = bytes

	bytes, err = ioutil.ReadFile(strings.TrimSpace(cfg.TPPTransportCertPath))
	if err != nil {
		return errors.Wrap(err, "Read transport cert path")
	}
	appState.TPPTransportCert = bytes

	appState.ASPSPOIDCWellKnownEndpoint = cfg.ASPSPOIDCWellKnownEndpoint
	appState.ASPSPOrganisationID = cfg.ASPSPOrganisationID
	appState.TPPOrganisationID = cfg.TPPOrganisationID
	appState.TPPSoftwareID = cfg.TPPSoftwareID
	appState.TPPSigningKeyID = cfg.TPPSigningKID
	appState.TPPSoftwareStatementAssertion = cfg.TPPSSA
	appState.DCRClaimAudience = cfg.DCRClaimAudience
	appState.TPPRedirectURLs = cfg.TPPRedirectURLs
	appState.TPPResponseTypes = cfg.TPPResponseTypes
	appState.TPPGrantTypes = cfg.TPPGrantTypes

	fmt.Println("Successfully loaded client configuration from file")

	return nil
}

func buildMenu() {
	menuItems = []MenuItem{
		{
			Label:       "Directory Enrolment",
			Description: "Enrol with a directory",
			Action:      directoryEnrollment,
		},
		{
			Label:       "Load Directory Configuration",
			Description: "Load the directory configuration as an alternate to Directory Enrollment step",
			Action:      loadDirectoryConfig,
		},
		{
			Label:       "Dynamic Registration",
			Description: "Register as TPP with ASPSP - Use values from previous step",
			Action:      dynamicRegistration,
		},
		{
			Label:       "User Authentication / Consent",
			Description: "Request PSU to authenticate against ASPSP and consent to TPP access",
			Action:      userAuthenticationConsent,
		},
		{
			Label:       "Account Access",
			Description: "Perform actions on the PSU account held at ASPSP",
			Action:      accountAccess,
		},
		{
			Label:       "Exit",
			Description: "Quit the application",
			Action:      exit,
		},
	}

	templates := promptui.SelectTemplates{
		Active:   `→  {{ .Label | red | bold }}`,
		Inactive: `   {{ .Label | cyan }}`,
		Selected: `{{ .Description| cyan }}`,
		Details:  `{{ .Description}}`,
	}

	menuList = promptui.Select{
		Size:      10,
		Label:     "Action",
		Items:     menuItems,
		Templates: &templates,
	}
}

func debugAppFlow() {
	fmt.Println("*************************************************")
	fmt.Println("Debug mode enabled")
	fmt.Println("*************************************************")

	if err := loadDirectoryConfigFile("hsbc.json"); err != nil {
		logrus.WithError(err).Fatal("loadDirectoryConfig()")
	}
	if err := dynamicRegistration(); err != nil {
		logrus.WithError(err).Fatal("dynamicRegistration()")
	}
}
